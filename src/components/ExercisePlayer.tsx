import React from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';

import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Slider from '@material-ui/core/Slider';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Input from '@material-ui/core/Input';

import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import PauseIcon from '@material-ui/icons/Pause';
import FastForwardIcon from '@material-ui/icons/FastForward';
import FastRewindIcon from '@material-ui/icons/FastRewind';
import RepeatIcon from '@material-ui/icons/Repeat';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import Exercise from '../MidiPlayer/Exercise';
import Time from '../utilities/Time';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
    toolbar: theme.mixins.toolbar,
    content: {
      display: 'flex',
      textAlign: 'center',
    },
    paper: {
      padding: theme.spacing(3),
      width: '100%',
    },
    startTime: {
      textAlign: 'left',
    },
    endTime: {
      textAlign: 'right',
    },
    wideMargin: {
      marginLeft: '24px',
      marginRight: '24px',
    },
    tempoControl: {
      width: '240px',
    },
    grid: {
      marginBottom: '24px',
    }
  }),
);

interface ExercisePlayerProps {
  exercise?: Exercise;
  isPlaying: boolean;
  handlePlay: () => void;
  currentTime?: number;
  totalTime?: number;
  tempo: number;
  handleTempoChange: (newTempo: number) => void;
}

const ExercisePlayer: React.FC<ExercisePlayerProps> = (props: ExercisePlayerProps) => {
  const classes = useStyles({});
  const minTempo = 20;
  const maxTempo = 300;

  const renderPlayButton = () => {
    if (props.isPlaying) {
      return <PauseIcon fontSize='large' />
    } else {
      return <PlayArrowIcon fontSize='large' />
    }
  }

  const calculateProgress = (): number => {
    if (!props.currentTime || !props.totalTime) {
      return 0;
    }

    let current = Math.round(props.currentTime!);
    let total = Math.round(props.totalTime!);

    return current / total * 100;
  }

  const handleSliderChange = (event: any, newValue: number | number[]) => {
    if (typeof newValue !== 'number') {
      return;
    }
    let newTempo = (maxTempo-minTempo)*(newValue/100) + minTempo;
    props.handleTempoChange(newTempo);
  };

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    let newTempo = event.target.value === '' ? 120 : Number(event.target.value);
    props.handleTempoChange(newTempo);
  };

  return (
    <main className={classes.root}>
      <div className={classes.toolbar} />
      <div className={classes.content}>
        <Grid container spacing={0}>
          <Paper className={classes.paper}>
            <Grid container item xs={12} spacing={0} alignItems='center' justify='center' className={classes.grid}>
              <Grid item className={classes.wideMargin}>
                <Typography>
                  Starting Key
                </Typography>
                <Typography>
                  <IconButton><ChevronLeftIcon /></IconButton>
                  C4
                  <IconButton><ChevronRightIcon /></IconButton>
                </Typography>
              </Grid>
              <Grid item className={classes.wideMargin}>
                <Typography variant='h4'>
                  C4
                </Typography>
              </Grid>
              <Grid item className={classes.wideMargin}>
                <Typography>
                  Ending Key
                </Typography>
                <Typography>
                  <IconButton><ChevronLeftIcon /></IconButton>
                  C5
                  <IconButton><ChevronRightIcon /></IconButton>
                </Typography>
              </Grid>
            </Grid>
            <Grid container item xs={12} spacing={0} className={classes.grid}>
              <Grid item xs={12}>
                <IconButton color='primary'>
                  <RepeatIcon />
                </IconButton>
              </Grid>
            </Grid>
            <Grid container item xs={12} spacing={0} className={classes.grid}>
                <Grid item xs={12}>
                  <Slider value={calculateProgress()} />
                </Grid>
                <Grid item xs={6} className={classes.startTime}>
                  {(props.currentTime) ? Time.formatSecondsAsTime(props.currentTime!) : '00:00'}
                </Grid>
                <Grid item xs={6} className={classes.endTime}>
                  {(props.totalTime) ? Time.formatSecondsAsTime(props.totalTime!) : '00:00' }
                </Grid>
            </Grid>
            <Grid container item xs={12} spacing={0} className={classes.grid}>
              <Grid item xs={12}>
                <Typography>
                  {(props.exercise) ? props.exercise.name : 'Select an exercise'}
                </Typography>
              </Grid>
            </Grid>
            <Grid container item xs={12} spacing={0} className={classes.grid}>
              <Grid item xs={12}>
                <IconButton color='primary' className={classes.wideMargin} disabled={!props.exercise}>
                  <FastRewindIcon fontSize='large' />
                </IconButton>
                <IconButton color='primary' className={classes.wideMargin} disabled={!props.exercise} onClick={props.handlePlay}>
                  {renderPlayButton()}
                </IconButton>
                <IconButton color='primary' className={classes.wideMargin} disabled={!props.exercise}>
                  <FastForwardIcon fontSize='large' />
                </IconButton>
              </Grid>
            </Grid>
            <Grid container item xs={12} spacing={0} justify='center'>
              <Grid item xs={12}>
                <Typography id="tempo-slider" align='center'>
                  Tempo
                </Typography>
              </Grid>
              <Grid container item spacing={2} alignItems='center' className={classes.tempoControl}>
                <Grid item xs>
                  <Slider
                    value={props.tempo/(maxTempo-minTempo)*100}
                    onChange={handleSliderChange}
                    aria-labelledby="tempo-slider"
                />
                </Grid>
                <Grid item>
                  <Input
                    value={props.tempo}
                    margin="dense"
                    onChange={handleInputChange}
                    inputProps={{
                      step: 5,
                      min: minTempo,
                      max: maxTempo,
                      type: 'number',
                      'aria-labelledby': 'tempo-slider',
                    }}
                  />
                </Grid>
              </Grid>
            </Grid>
          </Paper>
        </Grid>
      </div>
    </main>
  );
};

export default ExercisePlayer;