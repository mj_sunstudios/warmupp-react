class Time {
  /**
   * Given a number representing number of seconds, convert
   * to a string formatted as 'hh:mm:ss'
   * @param time the time in number of seconds
   */
  static formatSecondsAsTime(time: number): string {
    time = Math.round(time);
    let seconds = time % 60;
    let minutes = Math.trunc(time / 60) % 60;
    let hours = Math.trunc((time / 60) / 60);

    let dispTime = seconds.toString().padStart(2, '0');
    dispTime = minutes.toString().padStart(2, '0') + ':' + dispTime;
    if (hours > 0) {
      dispTime = hours.toString().padStart(2, '0') + ':' + dispTime;
    }

    return dispTime;
  }
}

export default Time;