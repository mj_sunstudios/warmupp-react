import React, { useEffect, useState } from 'react';
import { createMuiTheme, makeStyles, Theme } from '@material-ui/core/styles';
import { ThemeProvider, createStyles } from '@material-ui/styles';
import './App.css';
import ResponsiveDrawer from './components/ResponsiveDrawer';
import ExercisePlayer from './components/ExercisePlayer';

import Exercise, { IJSONExercise } from './MidiPlayer/Exercise';
import MidiSoundPlayer from './MidiPlayer/MidiSoundPlayer';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#00B0DE',
    },
    secondary: {
      main: '#00B0DE',
    },
  },
  typography: {
    fontFamily: '"Helvetica Neue", "Segoe UI", "Tahoma", "Ariel"',
  },
});

const useStyles = makeStyles((theme: Theme) => 
  createStyles({
    root: {
      display: 'flex',
    }
  }),
);



const App: React.FC = () => {
  const classes = useStyles({});
  const [ exercises, setExercises ] = useState<Exercise[]>([]);
  const [ player ] = useState(new MidiSoundPlayer());
  const [ isPlaying, setIsPlaying ] = useState(false);
  const [ currentExercise, setCurrentExercise ] = useState<Exercise>();
  const [ currentTime, setCurrentTime ] = useState<number>();
  const [ totalTime, setTotalTime ] = useState<number>();
  const [ tempo, setTempo ] = useState<number>(120);

  let interval: NodeJS.Timeout | undefined = undefined;

  const loadExercise = (exercise: Exercise) => {
    player.loadFile(exercise.filename, {
      onLoadCallback: () => {
        setTotalTime(player.getTotalTime());
      },
      onEndCallback: () => {
        setIsPlaying(false);
        if (interval) {
          clearInterval(interval);
          interval = undefined;
        }
      }
    });
    setCurrentExercise(exercise);
  };

  const handlePlay = () => {
    if (!currentExercise) {
      return;
    }
    if (isPlaying) {
      player.pause();
      setIsPlaying(false);
      if (interval) {
        clearInterval(interval);
        interval = undefined;
      }
      
    } else {
      player.play();
      setIsPlaying(true);
      interval = setInterval(() => {
        setCurrentTime(player.getCurrentTime());
      }, 1000);
    }
  }

  const handleTempoChange = (newTempo: number) => {
    setTempo(newTempo);
  };


  useEffect(() => {
    Exercise.getExerciseJSON().then(function(exerciseJSON: IJSONExercise[]) {
      let loadedExercises = exerciseJSON.map((json: IJSONExercise) => {
        return Exercise.fromJSON(json);
      });
      setExercises(loadedExercises);
    });
  }, []);

  return (
    <div className="App">
      <ThemeProvider theme={theme}>
        <div className={classes.root}>
          <ResponsiveDrawer exercises={exercises} exerciseOnClick={loadExercise} />
          <ExercisePlayer 
            isPlaying={isPlaying} 
            handlePlay={handlePlay} 
            exercise={currentExercise}
            currentTime={currentTime}
            totalTime={totalTime}
            tempo={tempo}
            handleTempoChange={handleTempoChange} />
        </div>
      </ThemeProvider>
    </div>
  );
}

export default App;
